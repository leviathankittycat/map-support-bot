# MAP Support Bot

This is the code for MSC Chat's MSC Bot!

# Roadmap

The two main objectives are:

- Creating a general library similar to something like Discord.js
- Converting the code to TypeScript

# How to contribute

Anyone is welcome to contribute to the bot! :D

If there's a feature you'd like to be added, create an Issue. We can then
discuss your idea 😃 Anyone can also program a feature (maybe one that solves an
issue) by forking this repository and then making a merge request to the `dev`
branch. 😀

## Overview of repository structure

The `app` folder:

- `index.js`: This file is the main brains of the code. Most of this will
  eventually be moved to its own library (see Roadmap)
- `utils`: This folder houses various utility functions
- `commands`: This folder contains all the commands the bot has.
- - `help.js`: This is a command that gives help on how to use other commands
- - There are various folders inside here, each folder represents a group of
    commands.

## Setting up the development environment

**_This section needs expanding on_**

- Setup a personal Rocket Chat instance

## Contributing anonymously

**_This section needs expanding on_**

Due to the nature of this topic, you might want to make sure that your
contributions aren't tied to your real life identity.

- Make sure that your GitLab account isn't tied to your IRL identity
- When commiting staged changes, make sure that your git config `user.email` and
  `user.name` are set to anonymous values. You can do this on a per repository
  basing with these commands: `git config --local user.name "My Name"` and
  `git config --local user.email "My email@example.com"`

## Coding Style

**_This section needs expanding on_**

## Service Set-up notes

mscbot.logrotate (gets installed to /etc/logrotate.d/mscbot)

**_This section needs expanding on_**