const getUserByUsername = require('../../utils/getUserByUsername')
const searchUser = require('../../utils/searchUser')

async function hug({ bot, message, context }) {
  const roomID = message.rid
  
  try {
    const user = await getUserByUsername(message.u.username)
    // Get the arguments
    let targetUser = context.argumentList[0]
    targetUser = await searchUser(targetUser)

    // Send the message
    message = await bot.sendToRoom(`_Hugs ${targetUser.name}! :hug:_`, roomID)
  } catch (err) {
    console.error(err)
    await bot.sendToRoom(
      'Command error',
      roomID
    )
  }
}

module.exports = {
  description: 'Hug someone!',
  help: `${process.env.ROCKETCHAT_PREFIX} hug <user>`,
  call: hug,
}
