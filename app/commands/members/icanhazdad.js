const fetch = require('isomorphic-fetch')

async function icanhazdad({ bot, message, context }) {
  const roomID = message.rid

  try {
    fetch('https://v2.jokeapi.dev/joke/Any?blacklistFlags=nsfw,religious,political,racist,sexist,explicit&type=single', {
      headers: { Accept: 'application/json' },
    })
      .then(async (response) => {
        console.log(response)
        if (response.status == 200) {
          return response.json()
        }
      })
      .then(async (joke) => await bot.sendToRoom(joke.joke, roomID))

    // Send the message
    //message =

    return true
  } catch (err) {
    console.error(err)
    await bot.sendToRoom(
      'Command error',
      roomID
    )

    return false
  }
}

module.exports = {
  description: 'Get a dad joke!',
  help: `${process.env.ROCKETCHAT_PREFIX} icanhazdad`,
  cooldown: 30,
  call: icanhazdad,
}
