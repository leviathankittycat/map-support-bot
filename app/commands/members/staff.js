const mention = require('./mention')

async function staff({ bot, message, context }) {
  const roomID = message.rid
  try {
    return await mention.call({ bot, message, context })
  } catch (err) {
    console.error(err)
    await bot.sendToRoom(
      'Command error',
      roomID
    )
    return false
  }
}

module.exports = {
  description: `Alias for '${process.env.ROCKETCHAT_PREFIX} mention'. ${mention.description}`,
  help: mention.help,
  call: staff,
  cooldown: mention.cooldown,
}
