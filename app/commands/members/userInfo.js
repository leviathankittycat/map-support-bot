const dayjs = require('dayjs')

const getUserByUsername = require('../../utils/getUserByUsername')
const getUserInfo = require('../../utils/getUserInfo')
const isMinor = require('../../utils/isMinor')
const userHasRole = require('../../utils/userHasRole')
const userHasRoles = require('../../utils/userHasRoles')
const roomInfo = require('../../utils/getRoom')

const emailsToString = (emails) => {
  let result = ''
  emails.forEach((email) => {
    result = `${result} ${email.address}`
    if (email.verified) {
      result = `${result} ✔️`
    } else {
      result = `${result} ❌`
    }
  })

  return result
}

const staffChannels = [
  '--staff-spam-log--'
]

async function userInfo({ bot, message, context }) {
  const roomID = message.rid

  try {
    const user = await getUserByUsername(message.u.username)
    // Get the targetUser object
    let targetUser = context.argumentList[0]
    targetUser = await getUserByUsername(targetUser)

    // Don't show email until we have a flag for it
    // Emails: ${emailsToString(targetUser.emails)}

    let userInfo = await getUserInfo(targetUser)
    let lastLogin = 'Never';
    if(typeof targetUser.lastLogin !== 'undefined'){
	  lastLogin=dayjs(targetUser.lastLogin).toString()
    }

    let staffUserInfo = `${'```'}
    Type:             ${userInfo.type}
    Name:             ${targetUser.name}
    Username:         ${targetUser.username}
    Roles:            ${targetUser.roles}

    Status:           ${userInfo.status}
    statusConnection: ${userInfo.statusConnection}
          
    Number of Rooms:  ${userInfo.rooms.length}
    utcOffset:        ${userInfo.utcOffset}
    Active:           ${userInfo.active}
    User ID:          ${targetUser._id}

    Status Text:      ${userInfo.statusText}

    Last Login:       ${lastLogin}
    Created At:       ${dayjs(userInfo.createdAt).toString()}
${'```'}
    `
    let memberUserInfo = `${'```'}
    Name:             ${targetUser.name}
    Username:         ${targetUser.username}
    Roles:            ${targetUser.roles}
    Status Text:      ${userInfo.statusText}
    Created At:       ${dayjs(userInfo.createdAt).toString()}
${'```'}
    `
    let response = ""

    let room = await roomInfo({roomId: roomID})

    if (userHasRoles(user, ['admin', 'Global Moderator', 'Guide']) && (message.roomType === 'd' || staffChannels.includes(room.fname))) {
      response = staffUserInfo
    } else {
      response = memberUserInfo
    }

    const sentMsg = await bot.sendToRoom(response, roomID)
  } catch (err) {
    console.error(err)
    await bot.sendToRoom(
      'Command error',
      roomID
    )
  }
}

module.exports = {
  description: 'Get information about a specific member.',
  help: `${process.env.ROCKETCHAT_PREFIX} userInfo <username>`,
  call: userInfo
}
