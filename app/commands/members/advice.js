const fetch = require('isomorphic-fetch')

async function advice({ bot, message, context }) {
  const roomID = message.rid
  
  try {
    fetch('https://api.adviceslip.com/advice')
      .then(async (response) => {
        if (response.status == 200) {
          return response.json()
        }
      })
      .then(async ({ slip }) => await bot.sendToRoom(slip.advice, roomID))

    // Send the message
    //message = await bot.sendToRoom(`_Trivia? ${targetUser.name}!_`, roomID)

    return true
  } catch (err) {
    console.error(err)
    await bot.sendToRoom(
      'Command error',
      roomID
    )

    return false
  }
}

module.exports = {
  description: 'Get advice!',
  help: `${process.env.ROCKETCHAT_PREFIX} advice`,
  cooldown: 30,
  call: advice,
}
