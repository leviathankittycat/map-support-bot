const getUsersInRole = require('../../utils/getUsersInRole')

const MOD = ['moderator', 'moderators', 'mod', 'mods', 'global moderator']
const ADMIN = ['admin', 'admins', 'administrator', 'administrators']
const GUIDE = ['guide', 'guides']

async function mention({ bot, message, context }) {
  const roomID = message.rid

  try {
    // Get the arguments
    let targetRole = context.argumentList[0]

    // Set up variables
    let users = []
    let messageToSend = ''

    // Was a role specified?
    if (targetRole !== undefined) {
      // Start checking which role was specified, then get users in that role
      if (MOD.includes(targetRole.toLowerCase())) {
        messageToSend = `Mentioning all moderators & administrators: `
        users = [...users, ...(await getUsersInRole('Global Moderator')).users]
        users = [...users, ...(await getUsersInRole('admin')).users]
      } else if (ADMIN.includes(targetRole.toLowerCase())) {
        messageToSend = `Mentioning all administrators: `
        users = (await getUsersInRole('admin')).users
      } else if (GUIDE.includes(targetRole.toLowerCase())) {
        messageToSend = `Mentioning all guides: `
        users = (await getUsersInRole('Guide')).users
      } else {
        // If no valid role, tell the user
        await bot.sendToRoom(
          'You can only mention admins, mods, or guides with this command.',
          roomID,
        )
        return false
      }
    } else {
      messageToSend = `Mentioning all staff members: `
      // No role specified, mention all staff members
      users = [...users, ...(await getUsersInRole('Global Moderator')).users]
      users = [...users, ...(await getUsersInRole('admin')).users]
      users = [...users, ...(await getUsersInRole('Guide')).users]
    }

    // Prepare message
    for (let user of users) {
      messageToSend = `${messageToSend} @${user.username} `
    }

    // Send the message
    await bot.sendToRoom(messageToSend, roomID)

    return true
  } catch (err) {
    console.error(err)
    await bot.sendToRoom(
      'Command error',
      roomID
    )

    return false
  }
}

module.exports = {
  description: 'Mention all staff members.',
  help: `${process.env.ROCKETCHAT_PREFIX} mention <optional admin|moderator|guide>`,
  call: mention,
  cooldown: 60 * 30,
}
