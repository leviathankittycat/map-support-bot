const getUserByUsername = require('../../utils/getUserByUsername')

const addUserToRole = require('../../utils/addUserToRole')
const userHasRole = require('../../utils/userHasRole')

async function addRole({ bot, message, context }) {
  const roomID = message.rid

  try {
    const user = await getUserByUsername(message.u.username)
    
    // Get the arguments object
    let targetRole = context.argumentList[0]
    let targetUser = context.argumentList[1]
    targetUser = await getUserByUsername(targetUser)

    // Check if the targetUser does not already have the role
    if (!userHasRole(targetUser, targetRole)) {
      // Add the role to targetUser
      await bot.sendToRoom(
        `Giving ${targetUser.name} the ${targetRole} role...`,
        roomID,
      )
      await addUserToRole(targetUser, targetRole)
    } else {
      // User already has the role, no need to do anything else
      const response = targetUser.name + ' already has the MAP role'
      const sentMsg = await bot.sendToRoom(response, roomID)
      return
    }
  } catch (err) {
    console.error(err)
    await bot.sendToRoom(
      'Command error occurred',
      roomID
    )
  }
}

module.exports = {
  description: 'Give a member a role.',
  help: `${process.env.ROCKETCHAT_PREFIX} addRole <role> <username>`,
  requireOneOfRoles: ['admin'],
  call: addRole,
}
