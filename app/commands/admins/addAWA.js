const getUserByUsername = require('../../utils/getUserByUsername')
const getChannelsByChannelName = require('../../utils/getChannelsByChannelName')

const addUserToRole = require('../../utils/addUserToRole')
const userHasRole = require('../../utils/userHasRole')

const inviteUserToChannels = require('../../utils/inviteUserToChannels')

const getGroupsByGroupName = require('../../utils/getGroupsByGroupName')
const inviteUserToGroups = require('../../utils/inviteUserToGroups')

const defaultChannels = [
  'comfy-cafe',
  'food',
  'fun-and-games',
  'general',
  'humor-and-jokes',
  'in-the-news',
  'life-experiences',
  'media',
  'miscellaneous',
  'movies-and-tv',
  'music',
  'serious',
  'social-media',
  'spam',
  'suggestions-discussions',
  'suggestions',
  'video-games',
  'the-learning-corner',
  'literature',
  'creativity-corner',
  'no-crushing',
  'stem',
  'support-sessions',
]

const privateGroups = [
  'information',
  'private-announcements'
]

async function addAWA({ bot, message, context }) {
  const roomID = message.rid

  try {
    const user = await getUserByUsername(message.u.username)
    
    // Get the targetUser object
    let targetUser = context.argumentList[0]
    targetUser = await getUserByUsername(targetUser)

    // Check if the targetUser does not already have the MAP role
    if (!userHasRole(targetUser, 'AWA')) {
      // Add the MAP role to targetUser
      await bot.sendToRoom(
        'Giving ' + targetUser.name + ' the AWA role...',
        roomID,
      )
      await addUserToRole(targetUser, 'AWA')
    }

      //Invite user to the list of private groups
      await bot.sendToRoom(
        'Inviting ' + targetUser.name + ' to the default private channels...',
        roomID,
      )

      const privateGroupIds = await getGroupsByGroupName(privateGroups)
      await inviteUserToGroups(targetUser, privateGroupIds)

      await bot.sendToRoom(
        'Finished inviting ' + targetUser.name + ' to the default private channels!',
        roomID,
      )

      // Invite the targetUser to default channels
      let channels = await getChannelsByChannelName(defaultChannels)
      await bot.sendToRoom(
        'Inviting ' + targetUser.name + ' to the default AWA channels...',
        roomID,
      )
      let invites = await inviteUserToChannels(targetUser, channels)
      await bot.sendToRoom(
        'Finished inviting ' +
          targetUser.name +
          ' to the default AWA channels! :D',
        roomID,
      )
  } catch (err) {
    console.error(err)
    await bot.sendToRoom(
      'Command Error occurred',
      roomID
    )
  }
}

module.exports = {
  description:
    'Give a non-map the AWA (Allies-With-Access) role and invite them to the appropriate channels.',
  help: `${process.env.ROCKETCHAT_PREFIX} addAWA <username>`,
  requireOneOfRoles: ['admin'],
  call: addAWA,
}
