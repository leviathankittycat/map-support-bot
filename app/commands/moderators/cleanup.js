const dayjs = require('dayjs')
const relativeTime = require('dayjs/plugin/relativeTime')
dayjs.extend(relativeTime)

const getUserByUsername = require('../../utils/getUserByUsername')

const getMembersInRoom = require('../../utils/getMembersInRoom')
const removeUserFromChannels = require('../../utils/removeUserFromChannels')
const inviteUserToChannel = require('../../utils/inviteUserToChannel')
const getUserInfo = require('../../utils/getUserInfo')
const getRooms = require('../../utils/getRooms')

const prescreeningID = 'yCJaDGjDmrc3RXuaE'
const SHOULD_REMOVE = true

async function cleanup({ bot, message, context }) {
  const roomID = message.rid

  try {
    const user = await getUserByUsername(message.u.username)

    // Get the number of days limit
    let daysLimit = context.argumentList[0]

    let result = await getMembersInRoom({
      roomName: 'onboarding',
      count: 0,
    })
    let members = result.members

    await bot.sendToRoom(`Checking ${members.length} members in total...`, roomID)

    let progress = 0
    for (let member of members) {
      progress += 1

      if (progress === 50) {
        progress = 0
        await bot.sendToRoom(`Am still checking...`, roomID)
      }

      const userInfo = await getUserInfo(member)
      const { lastLogin, roles, active, rooms } = userInfo

      // Check if user has any others roles than the 'user' role
      if ((!roles.includes("MAP") && !roles.includes("Non-MAP") && !roles.includes("DM") && !roles.includes("AWA")) && active === true) {
        let past = dayjs().subtract(daysLimit, 'days')

        if (past.isAfter(lastLogin)) {
          console.log(lastLogin)

          // Don't kick the user from direct messages
          let roomsToKickFrom = userInfo.rooms.filter((room) => room.t !== 'd')

          // Format the list to work with the getRooms function
          roomsToKickFrom = await getRooms(
            roomsToKickFrom.map((room) => ({ roomId: room.rid })),
          )

          // Don't kick the user from default channels
          roomsToKickFrom = roomsToKickFrom.filter((room) => room != null && !room.default)

          await bot.sendToRoom(
            `${member.name} last logged in ${dayjs(
              lastLogin,
            ).fromNow()}, so I'm moving them to prescreening.`,
            roomID,
          )

          if (SHOULD_REMOVE) {
            let kicks = await removeUserFromChannels(member, roomsToKickFrom)
            kicks.forEach((kick) => {
              console.log('THIS IS A KICK OR AN ERROR FOR DEBUGGING:', kick)
            })

            await bot.sendToRoom(
              'Finished removing ' + member.name + ' from all the rooms! 🔨',
              roomID,
            )

            // Add the user to pre-screening
            let invite = await inviteUserToChannel(member, {
              _id: prescreeningID,
            })

            await bot.sendToRoom(
              'Added ' + member.name + ' to prescreening.',
              roomID,
            )
          }
        }
      }
    }

    await bot.sendToRoom(`Finished!`, roomID)
  } catch (err) {
    console.error(err)
    await bot.sendToRoom(
      'Command error',
      roomID
    )
  }
}

module.exports = {
  description:
    "Remove members with only the `user` role from onboarding, who haven't signed on in a specific time period.",
  help: `${process.env.ROCKETCHAT_PREFIX} cleanup <days>`,
  requireOneOfRoles: ['admin', 'Global Moderator', 'bot'],
  call: cleanup,
}
