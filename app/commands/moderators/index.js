const unmap = require('./unMAP')
const cleanup = require('./cleanup')

module.exports = {
  commands: { 'Moderators Commands': { unmap, cleanup } },
}
