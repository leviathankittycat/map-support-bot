const getUserByUsername = require('../../utils/getUserByUsername')
const getChannelsByChannelName = require('../../utils/getChannelsByChannelName')

const removeUserFromChannels = require('../../utils/removeUserFromChannels')

const addUserToRole = require('../../utils/addUserToRole')
const userHasRole = require('../../utils/userHasRole')

const inviteUserToChannels = require('../../utils/inviteUserToChannels')
const getGroupsByGroupName = require('../../utils/getGroupsByGroupName')
const inviteUserToGroups = require('../../utils/inviteUserToGroups')
const { sendDirectToUser } = require('@rocket.chat/sdk/dist/lib/driver')

const defaultChannels = [
  '--announcements--',
  'childlike-self-concept-map',
  'comfy-cafe',
  'creativity-corner',
  'food',
  'fun-and-games',
  'general',
  'humor-and-jokes',
  'in-the-news',
  'introductions',
  'life-experiences',
  'literature',
  'map-talk',
  'media',
  'mingled-support',
  'miscellaneous',
  'movies-and-tv',
  'music',
  'non-map-ally-chat',
  'onboarding-for-non-maps',
  'positivity',
  'requests-for-support-r1-map',
  'requests-for-support-r2-map',
  'serious',
  'social-media-with-allies',
  'spam',
  'stem',
  'suggestions',
  'suggestions-discussions',
  'support-sessions',
  'the-learning-corner',
  'venting-map',
  'video-games',
]

const minorGroups = [
  'minors'
]

const privateGroups = [
  'information',
  'private-announcements'
]

const channelsToRemovefrom = [
  'onboarding-serious'
]

async function addMAP({ bot, message, context }) {
  const roomID = message.rid

  try {
    const user = await getUserByUsername(message.u.username)

    // Get the targetUser object
    let targetUserName = context.argumentList[0]
    targetUser = await getUserByUsername(targetUserName)

    if (targetUser == null) {
      await bot.sendToRoom(
        'Could not find user ' + targetUserName+ ' or an API error occurred.',
        roomID,
      )
      return;
    }

    let removalChannels = await getChannelsByChannelName(channelsToRemovefrom)

    await bot.sendToRoom('Removing user from post onboarding channel(s)', roomID)
    let kicks = await removeUserFromChannels(targetUser, removalChannels)
    kicks.forEach((kick) => {
      console.log('THIS IS A KICK OR AN ERROR FOR DEBUGGING:', kick)
    })

    // Check if the targetUser does not already have the MAP role
    if (!userHasRole(targetUser, 'MAP')) {
      // Add the MAP role to targetUser
      await bot.sendToRoom(
        'Giving ' + targetUser.name + ' the MAP role...',
        roomID,
      )
      await addUserToRole(targetUser, 'MAP')
    }

      //Invite user to the list of private groups
      await bot.sendToRoom(
        'Inviting ' + targetUser.name + ' to the default private channels...',
        roomID,
      )

      const privateGroupIds = await getGroupsByGroupName(privateGroups)
      await inviteUserToGroups(targetUser, privateGroupIds)

      await bot.sendToRoom(
        'Finished inviting ' + targetUser.name + ' to the default private channels!',
        roomID,
      )

      if (userHasRole(targetUser, 'minor')) {
        //targetUser is a minor; add them to the #minors channel
        
        await sendDirectToUser('Adding ' + targetUser.username + ' to #minors / private group channel(s) :hansen:', message.u.username)
        const minorChannelIds = await getGroupsByGroupName(minorGroups)
        await inviteUserToGroups(targetUser, minorChannelIds)
        await sendDirectToUser('Added ' + targetUser.username + ' to #minors / private group channel(s) :hansen:', message.u.username)
      } else if (!userHasRole(targetUser, 'DM')) {
        await bot.sendToRoom(
          'Adding DM role to ' + targetUser.name,
          roomID,
        )
        await addUserToRole(targetUser, 'DM')
      }

      // Invite the targetUser to default channels
      await bot.sendToRoom(
        'Inviting ' + targetUser.name + ' to the default channels...',
        roomID,
      )
      let channels = await getChannelsByChannelName(defaultChannels)
      let invites = await inviteUserToChannels(targetUser, channels)
      invites.forEach((invite) => {
        console.log('THIS IS A INVITE OR AN ERROR FOR DEBUGGING:', invite)
      })
      await bot.sendToRoom(
        'Finished inviting ' + targetUser.name + ' to the default channels! Please check the list of optional rooms in the directory or #information if you would like to see even more! :D',
        roomID,
      )
    } catch (err) {
      console.log(err)
      await bot.sendToRoom(
        'Command error',
        roomID
      )
    }
}

module.exports = {
  description:
    'Give a member the MAP role and invite them to the appropriate channels.',
  help: `${process.env.ROCKETCHAT_PREFIX} addMAP <username>`,
  requireOneOfRoles: ['admin', 'Global Moderator', 'Guide'],
  call: addMAP,
}
