const getUserByUsername = require('../../utils/getUserByUsername')
const getChannelsByChannelName = require('../../utils/getChannelsByChannelName')

const addUserToRole = require('../../utils/addUserToRole')
const userHasRole = require('../../utils/userHasRole')

const inviteUserToChannels = require('../../utils/inviteUserToChannels')
const removeUserFromChannel = require('../../utils/removeUserFromChannel')

const ChannelsToInviteUserTo = ['onboarding', 'introductions', 'onboarding-serious']

async function onboard({ bot, message, context }) {
  const roomID = message.rid
  try {
    const roomName = message.roomName

    // Require command to be called in #--pre-screening--
    if (roomName !== 'pre-screening') {
      await bot.sendToRoom(
        'Command needs to be called in #--pre-screening--',
        roomID,
      )
      return
    }

    // Get the targetUser object
    let targetUser = context.argumentList[0]
    targetUser = await getUserByUsername(targetUser)

    // User can't have more than one role
    // this should ensure that the user only has the user role.
    // An attempt was made to compare:
    // targetUser.roles !== ['user']
    // But it did not work for some reason.

    // TODO: Look for specific roles rather than amount of roles
    if (targetUser.roles.length > 2) {
      await bot.sendToRoom(
        "Target user can't have any other role than the `user` and either `DM` or `minor` roles",
        roomID,
      )
      return
    }

    // Invite the targetUser to channels
    let channels = await getChannelsByChannelName(ChannelsToInviteUserTo)
    await bot.sendToRoom(
      'Inviting ' + targetUser.name + ' to the onboarding channels...',
      roomID,
    )
    let invites = await inviteUserToChannels(targetUser, channels)
    invites.forEach((invite) => {
      console.log('THIS IS A INVITE OR AN ERROR FOR DEBUGGING:', invite)
    })
    await bot.sendToRoom(
      'Finished inviting ' + targetUser.name + ' to the onboarding channels! :D',
      roomID,
    )

    await bot.sendToRoom('Removing user from #--pre-screening-- ...', roomID)
    let kicks = await removeUserFromChannel(targetUser, { _id: roomID })
    await bot.sendToRoom('User has been moved to onboarding! :D', roomID)
  } catch (err) {
    console.error(err)
    await bot.sendToRoom(
      'Command error',
      roomID
    )
  }
}

module.exports = {
  description: 'Onboard a member, moving them unto the onboarding stage',
  help: `${process.env.ROCKETCHAT_PREFIX} onboard <username>`,
  requireOneOfRoles: ['admin', 'Global Moderator', 'Guide'],
  call: onboard,
}
