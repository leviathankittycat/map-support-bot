const getUserByUsername = require('../../utils/getUserByUsername')
const getChannelsByChannelName = require('../../utils/getChannelsByChannelName')
const userHasRole = require('../../utils/userHasRole')
const inviteUserToChannels = require('../../utils/inviteUserToChannels')
const addUserToRole = require('../../utils/addUserToRole')

const defaultChannels = [
    'mingled-support',
    'onboarding'
]

async function addAlly({ bot, message, context }) {
    const roomID = message.rid
    try {
      const user = await getUserByUsername(message.u.username)

      // Get the targetUser object
      let targetUser = context.argumentList[0]
      targetUser = await getUserByUsername(targetUser)

      // Check if the targetUser has the Non-MAP role
      if (userHasRole(targetUser, 'Non-MAP')) {
          // Invite the targetUser to default channels
          let channels = await getChannelsByChannelName(defaultChannels)
          await bot.sendToRoom(
              'Adding ' + targetUser.name + ' to the Ally channels...',
              roomID,
            )
          let invites = await inviteUserToChannels(targetUser, channels)
          await bot.sendToRoom(
          'Finished inviting ' +
              targetUser.name +
              ' to the Ally channels! :D',
          roomID,
          )

          //Currently all allies are adults; but just in case.....
          if (!userHasRole(targetUser, 'minor') && !userHasRole(targetUser, 'DM')) {
            await bot.sendToRoom(
              'Adding DM role to ' + targetUser.name,
              roomID,
            )
            await addUserToRole(targetUser, 'DM')

            await bot.sendToRoom(
              'Added DM role to ' + targetUser.name + "; command complete :)",
              roomID,
            )
          }
      } else {
          // User does not have the Non-MAP role
          const response = targetUser.name + ' does not have the Non-MAP role'
          const sentMsg = await bot.sendToRoom(response, roomID)
          return
        }
    } catch (err) {
      console.error(err)
      await bot.sendToRoom(
        'Command error',
        roomID
      )
    }
}

module.exports = {
    description:
      'Add a non-map to the appropriate channels for an Ally.',
    help: `${process.env.ROCKETCHAT_PREFIX} addAlly <username>`,
    requireOneOfRoles: ['admin', 'Global Moderator', 'Guide'],
    call: addAlly,
  }