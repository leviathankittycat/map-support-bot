const { api } = require('@rocket.chat/sdk')

const getChannelByChannelName = async (channelName) => {
  try {
    let result = await api.get('channels.info', { roomName: channelName })
    if (result && result.success) {
      return result.channel
    } else {
      console.error(result)
    }
  } catch (err) {
    console.error(err)
    return err
  }
}

module.exports = getChannelByChannelName
