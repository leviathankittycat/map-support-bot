const { api } = require('@rocket.chat/sdk')

const getChannelByChannelID = async (ID) => {
  try {
    let result = await api.get('channels.info', { roomId: ID })
    if (result && result.success) {
      return result.channel
    } else {
      console.error(result)
    }
  } catch (err) {
    console.error(err)
    return err
  }
}

module.exports = getChannelByChannelID
