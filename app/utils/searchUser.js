const { api } = require('@rocket.chat/sdk')

const searchUser = async query => {
    try {
        // Search just by username
        let hardSearch = await api.get('users.get', { query });

        if (hardSearch && hardSearch.success) {
            return result.user
        } else {
            // Get list of users, search for name
            let usersList = await api.get("users.list", { query: {} });
            for (user in usersList) {
                if (user.name.includes(query) || user.username.includes(query)) {
                    return user
                }
            }
        }
    } catch (err) {
        console.error(err)
        return err
    }
}

module.exports = searchUser