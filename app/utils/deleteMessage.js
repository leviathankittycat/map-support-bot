const { api } = require('@rocket.chat/sdk')

const deleteMessage = async (roomId, msgId, asUser = false) => {
  try {
    let result = await api.post('chat.delete', { roomId, msgId, asUser })
    if (result && result.success) {
      return result.channel
    } else {
      console.error(result)
    }
  } catch (err) {
    console.errorr(err)
    return err
  }
}

module.exports = deleteMessage
