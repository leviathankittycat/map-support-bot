const { api } = require('@rocket.chat/sdk')

const getUsersInRole = async (role, roomId) => {
  try {
    let result = await api.get('roles.getUsersInRole', { role, roomId, count: 0 })
    if (result && result.success) {
      return result
    } else {
      console.error(result)
    }
  } catch (err) {
    console.error(err)
    return err
  }
}

module.exports = getUsersInRole
