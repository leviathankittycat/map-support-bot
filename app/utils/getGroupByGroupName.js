const { api } = require('@rocket.chat/sdk')

const getGroupByGroupName = async (groupName) => {
    try {
      let result = await api.get('groups.info', { roomName: groupName })
      if (result && result.success) {
        return result.group
      } else {
        console.error(result)
      }
    } catch(err) {
      console.error(err)
      return err
    }
  }

  module.exports = getGroupByGroupName