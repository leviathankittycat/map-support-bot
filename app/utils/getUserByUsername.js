const { api } = require('@rocket.chat/sdk')

const getUserByUsername = async username => {
  try {
    let result = await api.get('users.info', { username })

    //The SDK library for an invalid user (or other assorted error), will actually throw an exception internally and the promise will return as null.
    //TODO: Revisit with the latest versions of RC and SDK library
    if (result && result.success) {
      return result.user
    } else {
      console.error(result)
    }
  } catch (err) {
    console.error(err)
    return err
  }
}

module.exports = getUserByUsername
