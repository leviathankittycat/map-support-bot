const { api } = require('@rocket.chat/sdk')

const getUserInfo = async (user) => {
  try {
    let result = await api.get('users.info', {
      userId: user._id,
      fields: { userRooms: 1 },
    })
    if (result && result.success) {
      return result.user
    } else {
      console.error(result)
    }
  } catch (err) {
    console.error(err)
    return err
  }
}

module.exports = getUserInfo
