const { api } = require('@rocket.chat/sdk')

const getGroupByGroupName = require('./getGroupByGroupName')

const getGroupsByGroupName = async groupNames => {
  let groups = []
  for (let groupName of groupNames) {
    groups.push(await getGroupByGroupName(groupName))
  }
  return groups
}

module.exports = getGroupsByGroupName