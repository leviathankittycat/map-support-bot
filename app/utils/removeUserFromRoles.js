const { api } = require('@rocket.chat/sdk')

const removeUserFromRoles = async (user, rolesToRemove, roomId = undefined) => {
  try {
    let newRoles = user.roles.filter((role) => !rolesToRemove.includes(role))
    console.log(newRoles)
    let result = await api.post('users.update', {
      userId: user._id,
      data: {
        roles: newRoles,
      },
    })
    if (result && result.success) {
      return result.user
    } else {
      console.error(result)
    }
  } catch (err) {
    console.error(err)
    return err
  }
}

module.exports = removeUserFromRoles
