const { api } = require('@rocket.chat/sdk')

const doesUserExist = async username => {
    try {
        let success = false;
        let result = await api.get('users.info', { username })

        if (result != undefined) {
            success = result.success;
        }
    } catch (err) {
        console.error(err)
        return err
    }

    return success;
}

module.exports = doesUserExist