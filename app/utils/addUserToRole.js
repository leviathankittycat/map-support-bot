const { api } = require('@rocket.chat/sdk')

const addUserToRole = async (user, roleName, roomId = undefined) => {
  try {
    let result = await api.post('roles.addUserToRole', {
      roleName,
      username: user.username,
      roomId,
    })
    if (result && result.success) {
      return result.role
    } else {
      console.error(result)
    }
  } catch (err) {
    console.error(err)
    return err
  }
}

module.exports = addUserToRole
