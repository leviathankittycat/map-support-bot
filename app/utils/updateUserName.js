const { api } = require('@rocket.chat/sdk')

const getUserByUsername = require('./getUserByUsername')

const updateUserName = async (currentUserName, newUserName) => {
  try {
    const user = await getUserByUsername(currentUserName);

    let result = await api.post('users.update', {
        userId: user._id,
        data: {
          username: newUserName,
        },
      })
      
      if (result && result.success) {
        return result.user
      } else {
        console.error(result)
      }
    } catch (err) {
      console.error(err)
      return err
    }
}

module.exports = updateUserName