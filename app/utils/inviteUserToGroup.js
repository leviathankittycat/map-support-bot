const { api } = require('@rocket.chat/sdk')

const inviteUserToGroup = async (user, group) => {
  //Todo: We probs want to have what ever calls this to confirm whether the returned value is an error or not.

  try {
    let result = await api.post('groups.invite', {
      roomId: group._id,
      userId: user._id,
    })
    if (result.success) {
      return result.group
    } else {
      console.error(result)
    }
  } catch (error) {
    console.error(result)
    return error
  }
}

module.exports = inviteUserToGroup