const { api } = require('@rocket.chat/sdk')

const getUsersList = async (object) => {
  try {
    let result = await api.get('users.list', object)
    if (result && result.success) {
      return result
    } else {
      console.error(result)
    }
  } catch (err) {
    console.error(err)
    return err
  }
}

module.exports = getUsersList
